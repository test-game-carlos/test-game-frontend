import React, {useEffect} from 'react'
import GamesTable from './GamesTable'
import useGetGames from '../customHooks/useGetGames'

const GamesDashboard = () => {
    const [asyncGames, getGames] = useGetGames()
    
    useEffect(() => {
        getGames()
    }, [])

    return (
        <div>
            {asyncGames.itsOk && <GamesTable games = {asyncGames.value}/>}    
        </div>
    )
}
export default GamesDashboard