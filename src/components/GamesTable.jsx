import React from 'react'

const GamesTable = ({games}) => {
    return (
        <table>
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>URL</th>
                    <th>Descripción</th>
                    <th>Imagen</th>
                    <th>Estado</th>
                </tr>
            </thead>
            <tbody>
                {games.map((game) => {
                    <tr key={game.idGame}>
                        <td>{game.name_game}</td>
                        <td>{game.url_game}</td>
                        <td>{game.description_game}</td>
                        <td>{game.url_image}</td>
                        <td>{game.status_game}</td>    
                    </tr>
                })}
            </tbody>
        </table>
    )
}
export default GamesTable