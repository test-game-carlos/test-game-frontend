import './App.css';
import GamesDashboard from './components/GamesDashboard'

function App() {
  return (
    <div>
      <h3>Test Laravel Juegos</h3>
      <GamesDashboard />
    </div>
  );
}

export default App;
