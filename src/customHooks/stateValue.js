import {useState} from 'react'

export default (defaultValue)=> {
  const defaultState = {
    value: defaultValue,
    error: undefined,
    loading: defaultValue === null || defaultValue === undefined,
  }
  
  const [state, setState] = useState(defaultState)

  return {
    loading: state.loading,
    withError: state.loading === false && state.error,
    itsOk: state.loading === false && state.error === undefined,
    error: state.error,
    value: state.value,
    setLoading: ()=> setState({
      loading: true,
      error: undefined,
      value: undefined
    }),
    setValue: (value)=> setState({
      loading: false,
      error: undefined,
      value
    }),
    setError: (error)=> setState({
      loading: false,
      error,
      value: undefined
    })
  }
}