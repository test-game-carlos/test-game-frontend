import stateValue from './stateValue'
import {ENDPOINT} from '../constants/constants'

const useGetGames = () => {
  const asyncGames = stateValue()
  const fetchData = async () => {
    try {
      asyncGames.setLoading()
      const resp = await fetch(`${ENDPOINT}/api/games`)
      asyncGames.setValue(await resp.json())
    } catch(err) {
      asyncGames.setError(err)
    }
  }
  return [asyncGames, fetchData]
}
export default useGetGames